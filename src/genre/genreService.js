const baseUrl = '/sepotifay/api/genres';

const getAllGenre = async () => {
    const genres = await fetch(baseUrl);
    return await genres.json();
}

const createGenre = async (form) => {
    const genre = await fetch(baseUrl, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(form)
    })

    return await genre.json();
}

const updateGenre = async (form) => {
    const genre = await fetch(baseUrl, {
        method: 'PUT',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(form)
    })

    return await genre.json();
}

const deleteGenre = async (id) => {
    const genre = await fetch(baseUrl + `/${id}`, {
        method: 'DELETE',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        }
    })

    return genre;
}

export { getAllGenre, updateGenre, createGenre, deleteGenre };