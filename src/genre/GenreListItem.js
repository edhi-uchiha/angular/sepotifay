import React, { Component } from 'react';
import { Button } from 'react-bootstrap';

class GenreListItem extends Component {

    constructor(props) {
        super(props)
        this.state = {
            genre: this.props.genre,
            disable: true
        }
    }

    handleEdit = () => {
        this.props.editGenre({ ...this.state.genre });
        this.handleClick();
    }

    handleClick = () => {
        this.setState({ ...this.state, disable: !this.state.disable })
    }

    handleChange = (e) => {
        this.setState({ ...this.state, genre: { ...this.state.genre, name: e.target.value } })
    }

    handleDelete = () => {
        this.props.deleteGenre(this.state.genre.id);
    }

    render() {

        const ButtonEditOrSave = (props) => <Button onClick={() => props.handleClick()} style={{ flex: 1, height: 50, marginRight: 12 }}>{props.title}</Button>
        const ActionButton = () => this.state.disable ? <ButtonEditOrSave handleClick={this.handleClick} title="Edit" /> : <ButtonEditOrSave handleClick={this.handleEdit} title="Save" />
        return (
            <div style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',

            }}>
                <input
                    style={{
                        flex: 6,
                        height: 50,
                        paddingLeft: 12,
                        borderWidth: this.state.disable ? 0 : 1,
                        marginRight: 24
                    }}
                    name="editTodo"
                    value={this.state.genre.name}
                    disabled={this.state.disable}
                    onChange={this.handleChange}
                />
                <ActionButton />
                <Button style={{ flex: 1, height: 50, marginLeft: 12 }} onClick={this.handleDelete}>
                    Remove
                </Button>
            </div>
        );
    }
}

export default GenreListItem;