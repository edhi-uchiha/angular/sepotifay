import React, { Component } from 'react';
import { getAllGenre, createGenre, updateGenre, deleteGenre } from './genreService';
import InputTodoComponent from '../components/InputTodoComponent';
import ListGenre from './ListGenre';
import ToastComponent from '../components/ToastComponent';
import { Col } from 'react-bootstrap';

class GenreContainer extends Component {

    constructor(props) {
        super(props);

        this.state = {
            genres: [],
            newGenre: "",
            showToast: {
                visible: false,
                title: "",
                message: "",
                color: ""
            }
        }
    }

    loadData = () => {
        getAllGenre().then(res => {
            this.setState({ ...this.state, genres: res.data })
        })
    }

    handleChangeInput = (event) => {
        this.setState({ ...this.state, newGenre: event.target.value })
    }

    addNewGenre = () => {
        const form = { name: this.state.newGenre };

        createGenre(form).then(res => {

            if (res.status.code === 201) {

                this.showToast({ title: "Create Genre", message: `Genre successfully created.`, color: 'green' });
                this.setState({ ...this.state, newGenre: "", });
                this.loadData();
            }
        })
    }

    editGenre = (form) => {
        updateGenre(form).then(res => {

            if (res.status.code === 200) {

                this.showToast({ title: "Update Genre", message: `Genre successfully updated.`, color: 'green' });
                this.loadData();
            }
        })
    }

    removeGenre = (id) => {

        const onDelete = window.confirm(`Delete genre with id ${id}`);

        if (onDelete) {
            deleteGenre(id).then(response => {

                if (response.status === 204) {
                    this.showToast({ title: "Delete Genre", message: `Genre successfully deleted.`, color: 'green' });
                    this.loadData();
                }
            })
        }

    }

    hideToast = () => {
        this.setState({
            ...this.state, showToast: {
                ...this.state.showToast,
                visible: false,
            }
        })
    }

    showToast = ({ title, message, color }) => {
        this.setState({
            ...this.state, showToast: {
                visible: true,
                title,
                message,
                color
            }
        })
    }

    componentDidMount() {
        this.loadData();
    }

    render() {

        return (
            <div>
                <Col>
                    {/* Form Create Genre Component */}
                    <InputTodoComponent
                        newTodo={this.state.newGenre}
                        handleChangeInput={this.handleChangeInput}
                        addTodo={this.addNewGenre}
                        placeholder="New Genre"
                        title="Genre" />

                    {/* List Genre Component */}
                    <ListGenre genres={this.state.genres} editGenre={this.editGenre} deleteGenre={this.removeGenre} />
                </Col>
                <ToastComponent toast={this.state.showToast} hideToast={this.hideToast} />
            </div>
        );
    }
}

export default GenreContainer;