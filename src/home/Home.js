import React from 'react';
import { Jumbotron, Button } from 'react-bootstrap';

const Home = ({ handleLogout }) => {

    return (
        <Jumbotron>
            <h1>Welcome, Edi Murwanto </h1>
            <p>
                This is a simple hero unit, a simple jumbotron-style component for calling
                extra attention to featured content or information.
                  </p>
            <p>
                <Button variant="primary">Learn more</Button>
            </p>
            <p>
                <Button variant="primary" onClick={handleLogout}>Logout</Button>
            </p>
        </Jumbotron>
    );
};

export default Home;