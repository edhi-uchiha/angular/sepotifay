import React, { Component } from 'react';
import { Button } from 'react-bootstrap';

class InputTodoComponent extends Component {
    render() {

        const { newTodo, handleChangeInput, addTodo, placeholder, title } = this.props

        return (
            <>
                <h1 style={{ textAlign: 'left' }}>{title}</h1>
                <div style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',

                }}>
                    <input
                        name="newTodo"
                        style={{ flex: 6, height: 50, paddingLeft: 12, marginRight: 12 }}
                        placeholder={placeholder}
                        onChange={handleChangeInput}
                        value={newTodo}
                    />
                    <Button style={{ flex: 2, height: 50, marginLeft: 12 }} onClick={addTodo} disabled={!newTodo}>
                        Create
                    </Button>
                </div>
            </>
        );
    }
}

export default InputTodoComponent;