import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faImage } from '@fortawesome/free-regular-svg-icons';
import { Image, Button } from 'react-bootstrap';

const ButtonUpload = ({ onChange, photo, label: title, disable }) => {

    const hiddenFileInput = React.useRef(null);

    const handleUpload = event => {
        hiddenFileInput.current.click();
    }

    return (
        <>
            <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', height: 270 }}>
                {
                    !photo ? (
                        <label htmlFor='single'>
                            <FontAwesomeIcon icon={faImage} color='#3B5998' size='10x' />
                        </label>) :
                        <Image src={photo} thumbnail style={{ height: 270, marginBottom: 8 }} />
                }

            </div>
            <input
                type='file'
                ref={hiddenFileInput}
                style={{ display: 'none' }}
                onChange={onChange}
            />
            <Button onClick={handleUpload} disabled={disable}>{title}</Button>
        </>
    );
};

export default ButtonUpload;